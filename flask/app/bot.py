import os
import slack
import requests
from pathlib import Path
from dotenv import load_dotenv
from datetime import datetime, timedelta
from flask import Flask, request
from flask.wrappers import Response
from .config import servers, api_key

class handler:
    MINUTES = 5
    ERROR = False
    NOTIFY = False

env_path = Path('./app') / '.env'
load_dotenv(dotenv_path = env_path)
client = slack.WebClient(token=os.environ['SLACK_TOKEN'])

app = Flask(__name__)

@app.route('/health', methods = ['POST'])
def get_health(notify = True):
    for server in servers:
        status_code = 500
        res = requests.get(server['url'] + '/health', timeout=200)
        status_code = res.status_code
        date_time = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
        message = '#############--  '+date_time+'  --###############\n'
        
        if status_code == 200:
            message += server['name'] + " Server is working fine.\nI will check every 15 minutes..Don't worry!"
            handler.NOTIFY = False
            handler.MINUTES = 15
        else:
            message += server['name'] + " Server is not working.\nI will notify you every 5 minutes until the server works fine!"
            handler.NOTIFY = True
            handler.MINUTES = 5

        if notify == True or handler.NOTIFY == True:
            try:
                message += '\n' + res.json()['message']
            except:
                pass
            client.chat_postMessage(channel='#servers-health', text = message)
    
    return Response(), 200


@app.route('/send/message', methods = ['POST', 'GET'])
def post_message():
    data = request.json
    if 'api_key' not in data or data['api_key'] != api_key:
        return Response(), 403
    if 'message' not in data or data.get('message', '') == '':
        return Response(), 400
    message = data['message']
    client.chat_postMessage(channel='#servers-health', text = message)
    return Response(), 200


def scheduled_check():
    fire_at = datetime.now().replace(microsecond=0)
    while True:
        now = datetime.now().replace(microsecond=0)
        if now == fire_at:
            get_health(handler.NOTIFY)
            fire_at += timedelta(minutes = handler.MINUTES)
