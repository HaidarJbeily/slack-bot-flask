from app.bot import app, scheduled_check
from threading import Thread

@app.before_first_request
def thread_start():
    service = Thread(target=scheduled_check, args=(), daemon = True)
    service.start()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, threaded = True)